import React, { useContext } from 'react';
import { FEMALE, MALE, OTHER } from '../../constants/gender.constant';
import { CharactersContext } from '../../contexts/characters/CharactersContext';



const CharactersList = () => {
    const charactersContext = useContext(CharactersContext);

    return ( 
        <div>
            {console.log(charactersContext.characters)}
            {charactersContext.characters.map( character =>
                <div key={character.id}>
                    <h3>nom : {character.name}</h3>
                    <ul>
                        <h4>Informations générales</h4>
                        <li>race : {character.race.name}</li>
                        {character.gender === FEMALE ? 
                            <li>genre : féminin </li> : 
                        character.gender === MALE ? 
                            <li>genre: masculin</li> : 
                        character.gender === OTHER ? 
                            <li>genre : inconnu </li> : 
                        ''}
                        <li>classe : {character.role.name}</li>
                        <li>niveau : {character.level}</li>
                        <li>expérience : {character.experience} %</li>
                        <ul>
                            <h5>Altérations d'état</h5>
                            {character.states.map((state, index) => 
                            <li key={index}>{state}</li>
                        )}
                        </ul>
                    </ul>
                    <ul>
                        <h4>Statistiques</h4>
                        <li>vie : {character.stats.health.current} / {character.stats.health.max}</li>
                        <li>mana : {character.stats.mana.current} / {character.stats.mana.max}</li>
                        <li>dégats physique : {character.stats.attackDamages.current} / {character.stats.attackDamages.max}</li>
                        <li>dégats magique : {character.stats.magicDamages.current} / {character.stats.magicDamages.max}</li>
                        <li>armure : {character.stats.armor.current} / {character.stats.armor.max}</li>
                        <li>protection magique : {character.stats.magicResist.current} / {character.stats.magicResist.max}</li>
                        <li>vitesse d'attaque : {character.stats.attackSpeed.current} / {character.stats.attackSpeed.max}</li>
                    </ul>
                    <ul>
                        <h4>Equipement</h4>
                        <li>tête : {character.equipment.head}</li>
                        <li>mains : {character.equipment.hands}</li>
                        <li>corps : {character.equipment.body}</li>
                        <li>jambes : {character.equipment.legs}</li>
                        <li>pieds : {character.equipment.feet}</li>
                        <li>armes: {character.equipment.weapon}</li>
                        <li>bouclier: {character.equipment.shield}</li>
                    </ul>
                </div>
            )}
        </div>
    );
}
 
export default CharactersList;
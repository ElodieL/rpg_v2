import React from 'react';
import './Navbar.scss';
import { Link } from 'react-router-dom';

const Navbar = () => {
    return ( 
        <ul className="Navbar">
            <li>
                <Link to='/' className="Navbar__link">Home</Link>
            </li>
            <li>
                <Link to='/battle' className="Navbar__link">Battle</Link>
            </li>
            <li>
                <Link to='/characterMaker' className="Navbar__link">Designer</Link>
            </li>
            <li>
                <Link to='/characters' className="Navbar__link">Characters</Link>
            </li>
        </ul>
     );
}
 
export default Navbar;
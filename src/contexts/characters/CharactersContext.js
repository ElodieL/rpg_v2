import React, { createContext, useState } from 'react';
import { CHARACTERS_DATA } from '../../data/character.data';

export const CharactersContext = createContext({});

export const CharactersState = props => {
    const characters = CHARACTERS_DATA;
    const [teamState] = useState( { team: [] })
    return ( 
        <CharactersContext.Provider
            value= {{
                characters: characters,
                team: teamState.team,
            }}
        >
            {props.children}
        </CharactersContext.Provider>
     );
}
 

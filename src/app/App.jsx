import React from 'react';
import './App.scss';
import { BrowserRouter as Router }  from 'react-router-dom';
import Routing from './Routing';
import Navbar from '../components/navbar/Navbar';
import { CharactersState } from '../contexts/characters/CharactersContext';


function App() {
  return (
    <CharactersState>
      <div className="App">
        <Router>
          <header>
            <Navbar/>
          </header>
          <main>
            <Routing/>
          </main>
          <footer>

          </footer>
        </Router>
      </div>
    </CharactersState>
  );
}

export default App;

import React from 'react';
import { Switch, Route }  from 'react-router-dom';
import CharactersList from '../components/charactersList/CharactersList';

const Routing = () => {
    return ( 
        <Switch>
            <Route exact path='/' component={Home_Page}/>
            <Route exact path='/battle' component={Battle_Page}/>
            <Route exact path='/characterMaker' component={CharacterMaker_Page}/>
            <Route exact path='/characters' component={Characters_Page}/>
        </Switch>
    );
};
 
export default Routing;

const Home_Page = () => {
    return ( 
        <>
            <h1>Home</h1>
        </>
    );
};

const Battle_Page = () => {
    return ( 
        <>
            <h1>Battle</h1>
        </>
    );
};

const CharacterMaker_Page = () => {
    return (
        <>
            <h1>Character maker</h1>
        </>
    );
};

const Characters_Page = () => {
    return (
        <>
            <CharactersList/>
        
        </>
    );
};
export default class Role {
    type;
    name;
    equipmentsAvailable;
    abilitiesAvailable;
    constructor(type, name, equipmentsAvailable, abilitiesAvailable) {
        this.type = type;
        this.name = name;
        this.equipmentsAvailable = equipmentsAvailable;
        this.abilitiesAvailable = abilitiesAvailable;
    };
};
export default class Character {
    id;
    name;
    gender;
    states;
    level;
    experience;
    race;
    role;
    stats;
    equipment;
    abilities;
    constructor(id, name, gender, states, level, experience, race, role, stats, equipment, abilities = []) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.states = states;
        this.level = level;
        this.experience = experience;
        this.race = race;
        this.role = role;
        this.stats = stats;
        this.equipment = equipment;
        this.abilities = abilities;
    };

    getStats() {
        
    }

    levelUp() {

    }

    changeRole() {

    }

    changeEquipment() {

    }

    learnNewAbility() {

    }
};

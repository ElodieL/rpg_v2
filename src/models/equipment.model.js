export class EquipmentType {
    type;
    equipments;
    constructor(type, equipments) {
        this.type = type;
        this.equipments = equipments;
    };
};


export class Equipment {
    type;
    name;
    stats;
    effects;
    abilities;
    constructor(type, name, stats, effects, abilities) {
        this.type = type;
        this.name = name;
        this.stats = stats;
        this.effects = effects;
        this.abilities = abilities;
    }
}
export default class Race {
    type;
    name;
    stats;
    rolesAvailable;
    constructor(type, name, stats, rolesAvailable) {
        this.type = type;
        this.name = name;
        this.stats = stats;
        this.rolesAvailable = rolesAvailable;
    };
};
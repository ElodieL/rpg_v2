export const HEAD = 'HEAD';
export const BODY = 'BODY';
export const HANDS = 'HANDS';
export const LEGS = 'LEGS';
export const FEET = 'FEET';
export const WEAPON = 'WEAPON';
export const SHIELD = 'SHIELD';

export const HELMET = 'HELMET';
export const TIARA = 'TIARA';
export const ARMOR = 'ARMOR';
export const DRESS = 'DRESS';
export const PANTS = 'PANTS';
export const TIGHTS = 'TIGHTS';
export const BOOTS = 'BOOTS';
export const SANDALS = 'SANDALS';
import Role from '../models/role.model';
import * as role from '../constants/role.constant';

export const warrior = new Role(
    role.WARRIOR, 
    'Guerrier',
    null,
    null,
);

export const mage = new Role(
    role.MAGE,
    'Mage',
    null,
    null,
);

export const priest = new Role(
    role.PRIEST,
    'Prêtre',
    null,
    null,
);

export const ROLES_DATA = [
    warrior,
    mage,
    priest
];




import * as equipmentType from '../constants/equipmentType.constant';
import { Equipment } from '../models/Equipment.model';

export const helmet = new Equipment(
    equipmentType.HELMET, 
    {}, 
    [], 
    [])
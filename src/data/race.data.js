import Race from '../models/race.model';
import * as race from '../constants/race.constant';
import * as role from '../data/role.data';

export const elf = new Race(
    race.ELF, 
    'Elfe',
    {
        health: 100,
        mana: 25,
        attackDamages: 10,
        magicDamages: 15,
        armor: 5,
        magicResist: 5,
        attackSpeed: 5,
    }, 
    [role.priest, role.mage]
);

export const human = new Race(
    race.HUMAN, 
    'Humain', 
    {
        health: 115,
        mana: 20,
        attackDamages: 15,
        magicDamages: 10,
        armor: 5,
        magicResist: 5,
        attackSpeed: 5,
    },  
    [role.warrior, role.priest]
);

export const ent = new Race(
    race.ENT, 
    'Ent', 
    {
        health: 150,
        mana: 10,
        attackDamages: 10,
        magicDamages: 10,
        armor: 20,
        magicResist: 20,
        attackSpeed: 3,
    },  
    [role.mage, role.warrior]
);



export const RACES_DATA = [
    elf,
    human,
    ent,
];
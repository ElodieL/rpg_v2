import Character from '../models/character.model';
import * as gender from '../constants/gender.constant';
import * as state from '../constants/state.constant';
import * as race from './race.data';


const alex = new Character(
    1, 
    'Alex', 
    gender.MALE,
    [state.BLESSED, state.PARALYSED],
    1, 
    0, 
    race.human, 
    race.human.rolesAvailable[0], 
    {
        health: {
            max: race.human.stats.health, 
            current: race.human.stats.health
        },
        mana: {
            max: race.human.stats.mana, 
            current: race.human.stats.mana
        },
        attackDamages: {
            max: race.human.stats.attackDamages, 
            current: race.human.stats.attackDamages
        },
        magicDamages: {
            max: race.human.stats.magicDamages, 
            current: race.human.stats.magicDamages
        },
        armor: {
            max: race.human.stats.armor, 
            current: race.human.stats.armor
        },
        magicResist: {
            max: race.human.stats.magicResist, 
            current: race.human.stats.magicResist
        },
        attackSpeed: {
            max: race.human.stats.attackSpeed, 
            current: race.human.stats.attackSpeed
        },
    }, 
    {
        head: "casque",
        body: "armure",
        hands: "gants de cuir",
        legs: "pantalon de cuir",
        feet: "bottes de cuir",
        weapon: "épée de fer",
        shield: "bouclier de bois"
    }, 
    []
);

const karma = new Character(
    2,
    'Karma',
    gender.FEMALE,
    [state.DEAD],
    10,
    50,
    race.elf,
    race.elf.rolesAvailable[1],
    {
        health: {
            max: race.elf.stats.health, 
            current: race.elf.stats.health
        },
        mana: {
            max: race.elf.stats.mana, 
            current: race.elf.stats.mana
        },
        attackDamages: {
            max: race.elf.stats.attackDamages, 
            current: race.elf.stats.attackDamages
        },
        magicDamages: {
            max: race.elf.stats.magicDamages, 
            current: race.elf.stats.magicDamages
        },
        armor: {
            max: race.elf.stats.armor, 
            current: race.elf.stats.armor
        },
        magicResist: {
            max: race.elf.stats.magicResist, 
            current: race.elf.stats.magicResist
        },
        attackSpeed: {
            max: race.elf.stats.attackSpeed, 
            current: race.elf.stats.attackSpeed
        },
    }, 
    {
        head: "casque",
        hands: "gants de cuir",
        body: "armure",
        legs: "pantalon de cuir",
        feet: "bottes de cuir",
        weapon: "épée de fer",
        shield: "bouclier de bois"
    },
    []
);

const sephy = new Character(
    2,
    'Sephy',
    gender.OTHER,
    [state.POISON, state.PARALYSED],
    50,
    98,
    race.ent,
    race.ent.rolesAvailable[1],
    {
        health: {
            max: race.ent.stats.health, 
            current: race.ent.stats.health
        },
        mana: {
            max: race.ent.stats.mana, 
            current: race.ent.stats.mana
        },
        attackDamages: {
            max: race.ent.stats.attackDamages, 
            current: race.ent.stats.attackDamages
        },
        magicDamages: {
            max: race.ent.stats.magicDamages, 
            current: race.ent.stats.magicDamages
        },
        armor: {
            max: race.ent.stats.armor, 
            current: race.ent.stats.armor
        },
        magicResist: {
            max: race.ent.stats.magicResist, 
            current: race.ent.stats.magicResist
        },
        attackSpeed: {
            max: race.ent.stats.attackSpeed, 
            current: race.ent.stats.attackSpeed
        },
    }, 
    {
        head: "casque",
        hands: "gants de cuir",
        body: "armure",
        legs: "pantalon de cuir",
        feet: "bottes de cuir",
        weapon: "épée de fer",
        shield: "bouclier de bois"
    },
    []
);

export const CHARACTERS_DATA = [
    alex,
    karma,
    sephy
];